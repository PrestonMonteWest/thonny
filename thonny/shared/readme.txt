The modules in this directory should be imported as thonny.<module>
instead of thonny.shared.thonny.<module>

In frontend this is possible because of proxy modules in thonny root package.
In backend this is possible because this directory will be in path